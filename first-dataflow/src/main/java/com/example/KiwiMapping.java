package com.example;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.TextIO;
import com.google.cloud.dataflow.sdk.options.*;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.transforms.DoFnWithContext;
import com.google.cloud.dataflow.sdk.transforms.PTransform;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;

import java.util.ArrayList;


public class KiwiMapping {

    public static class Logger {
        public static void Log(String message) {
            System.out.println(message);
        }

        public static void Log(String message, Throwable exception) {
            System.out.println(message + " --- " + exception.toString());
        }
    }

    public interface KiwiMappingOptions extends PipelineOptions {

        @Description("Path of the file to read from")
        @Default.String("gs://kiwidata/*")
        String getInputFile();

        void setInputFile(String value);

        @Description("Path of the file to write to")
        @Validation.Required
        String getOutput();

        void setOutput(String value);
    }

    static TableSchema getBqSchema() {
        return new TableSchema().setFields(new ArrayList<TableFieldSchema>() {
            {
                add(new TableFieldSchema().setName("sid").setType("STRING"));
                add(new TableFieldSchema().setName("timestamp").setType("STRING"));
                add(new TableFieldSchema().setName("date_to").setType("TIMESTAMP"));
            }
        });
    }

    public static void main(String[] args) {
        KiwiMappingOptions options = PipelineOptionsFactory.fromArgs(args).withValidation()
                .as(KiwiMappingOptions.class);
        Pipeline p = Pipeline.create(options);

        //todo: there is a problem. dataflow currently does not support easy load of binary data.
        // custom source would need to be implemented
        PCollection<TableRow> kiwidata = p.apply("kiwidata", TextIO.Read.from("gs://kiwidata/*"))
                .apply(new MapLines());

        kiwidata.apply(BigQueryIO.Write
                .named("Bq Write")
                .to("dogwood-method-193116:kiwidata.data")
                .withSchema(getBqSchema())
                .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE)
                .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED));
        ;

        p.run();
    }

    public static class MapLines extends PTransform<PCollection<String>, PCollection<TableRow>> {
        public PCollection<TableRow> expand(PCollection<String> lines) {

            PCollection<TableRow> jsonLines = lines.apply(
                    ParDo.of(new KiwiMapping.ToJson()));

            // Convert lines of text into individual words.
            PCollection<TableRow> BQLines = lines.apply(
                    ParDo.of(new KiwiMapping.AsBigQueryRow()));
            return BQLines;
        }
    }

    public static class ToJson extends DoFn<String, TableRow> {
        @DoFnWithContext.ProcessElement
        public void processElement(ProcessContext c) {
            try {
                TableRow json = null; // run JSON deserializer

                c.output(json);

            } catch (Throwable ex) {
                Logger.Log("Processing of a record failed for:" + c.element(), ex);
            }
        }

        public BigQueryTableRow extractRow(TableRow json) {
            BigQueryTableRow row = new BigQueryTableRow();
            String request = (String) json.get("request");
            row.Sid = (String) json.get("sid");
            row.DateTo = getRequestParam(request, "dateTo");
            row.Timestamp = (String) json.get("timestamp");

            return row;
        }

        public String getRequestParam(String request, String paramName) {
            return null;
        }


    }

    public static class AsBigQueryRow extends DoFn<String, TableRow> {
        private static final long serialVersionUID = 0;

        @DoFnWithContext.ProcessElement
        public void processElement(ProcessContext c) {
            //todo: parse to json or figure out how to make dataflow do it - TableRow "fakes" json with accessible fields
            TableRow jsonRow = null;//c.element();
            // Setup a date formatter to parse the date appropriately

            try {

                TableRow bQueryRow = extractRow(jsonRow).getBqRow();

                c.output(bQueryRow);

            } catch (Throwable ex) {
                Logger.Log("Processing of a record failed for:" + c.element(), ex);
            }
        }

        public BigQueryTableRow extractRow(TableRow json) {
            BigQueryTableRow row = new BigQueryTableRow();
            String request = (String) json.get("request");
            row.Sid = (String) json.get("sid");
            row.DateTo = getRequestParam(request, "dateTo");
            row.Timestamp = (String) json.get("timestamp");

            return row;
        }

        public String getRequestParam(String request, String paramName) {
            return null;
        }


    }
}

