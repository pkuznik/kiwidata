package com.example;

import com.google.api.services.bigquery.model.TableRow;

public class BigQueryTableRow {
    String Sid;
    String Timestamp;
    String DateTo;

    public TableRow getBqRow(){
        TableRow row = new TableRow();
        row.set("sid", Sid);
        row.set("timestamp", Timestamp);
        row.set("date_to", DateTo);

        return row;
    }
}

